import pytest
from tests.fixtures.service_manager import *


@pytest.mark.fixture_data('start', 'tests/data/default.yaml')
def test_start_services(service_manager_fixture, start_order):
    output = service_manager_fixture.do()
    for i in range(len(start_order)):
        assert set(start_order[i]) == set(output[i])


@pytest.mark.fixture_data('stop', 'tests/data/default.yaml')
def test_stop_services(service_manager_fixture, stop_order):
    output = service_manager_fixture.do()
    for i in range(len(stop_order)):
        assert set(stop_order[i]) == set(output[i])


@pytest.mark.fixture_data('start', 'tests/data/cirular_dependency.yaml')
def test_circular_dependency(service_manager_fixture, circular_dependency):
    with pytest.raises(SystemExit) as e:
        service_manager_fixture.validate()
        assert circular_dependency in str(e)


@pytest.mark.fixture_data('start', 'tests/data/missing_dependency.yaml')
def test_missing_dependency(service_manager_fixture, missing_dependency):
    with pytest.raises(SystemExit) as e:
        service_manager_fixture.validate()
        assert missing_dependency in str(e)
