import pytest
import logging
from depmanager.service_manager import ServiceManager


@pytest.fixture
def service_manager_fixture(request):
    """
    Initialize the service manager, perform an action, return the result
    """
    marker = request.node.get_closest_marker("fixture_data")
    action = 'start'
    data_file = 'tests/data/default.yaml'
    if marker is not None:
        action = marker.args[0]
        data_file = marker.args[1]

    logging.basicConfig(
        level=logging.DEBUG, format="%(asctime)s [%(levelname)-5.5s] %(message)s")
    return ServiceManager(data_file, logging, action)


@pytest.fixture
def start_order():
    """
    The expected start order of services with default service.yaml file
    """
    return [
        ['mysql', 'elasticsearch', 'zookeeper'],
        ['kibana', 'hadoop-namenode'],
        ['hbase-master'],
        ['fullhouse'],
        ['dashboard']
    ]


@pytest.fixture
def stop_order():
    """
    The expected stop order of services with default service.yaml file
    """
    return [
        ['kibana', 'dashboard'],
        ['fullhouse'],
        ['mysql', 'elasticsearch', 'hbase-master'],
        ['hadoop-namenode'],
        ['zookeeper']
    ]


@pytest.fixture
def circular_dependency():
    """
    The expected error message with circular dependency
    """
    return "Circular dependency error for mysql"


@pytest.fixture
def missing_dependency():
    """
    The expected error message when dependency is missing
    """
    return "mysql is not defined"
