#!/usr/bin/env python
import argparse
import logging
from service_manager import ServiceManager


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('action', type=str,
                        help='Start or stop services')
    parser.add_argument('-f', '--service_file', default='services.yaml',
                        help='Path to service definition file')
    return parser.parse_args()


def main():
    args = get_args()
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s [%(levelname)-5.5s] %(message)s")
    if args.action in ['start', 'stop']:
        sm = ServiceManager(args.service_file, logging, action=args.action)
        out = sm.do()
        for output_item in out:
            logging.info(output_item)
    else:
        logging.error("Supported actions are start or stop")


if __name__ == '__main__':
    main()
