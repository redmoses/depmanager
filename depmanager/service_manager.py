import asyncio
from asyncio.tasks import sleep
import yaml
import time


class ServiceManager:
    def __init__(self, service_file, logger, action='start', selector='deps'):
        self.logger = logger
        self.service_file = service_file
        self.services = []
        self.action = action
        self.selector = selector
        self.completed = []
        self.output = []
        self.loop = asyncio.new_event_loop()

    def validate(self):
        all_services = self.read_services()
        for service in all_services:
            all_services[service]['required_by'] = []
            if 'deps' not in all_services[service].keys():
                all_services[service]['deps'] = []
            else:
                for dependency in all_services[service]['deps']:
                    if dependency not in all_services.keys():
                        raise SystemExit(f"The service {dependency} is not defined. It is required by {service}")

            for s in all_services:
                if 'deps' in all_services[s].keys():
                    if service in all_services[s]['deps']:
                        if s in all_services[service]['deps']:
                            raise SystemExit(f"Circular dependency error for {service}")
                        all_services[service]['required_by'].append(s)

        self.services = all_services
        return None

    def read_services(self):
        with open(self.service_file, 'r') as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def resolved(self, service):
        result = True
        for d in self.services[service][self.selector]:
            if d not in self.completed:
                result = False

        return result

    async def act(self, service):
        resolved = self.resolved(service)

        if resolved and service not in self.completed:
            self.output.append({
                'service': service,
                'time': int(time.time())
            })
            self.logger.info(f'{self.action.title()}ing service {service} ...')
            await asyncio.sleep(1)
            self.completed.append(service)
            services_to_act = []

            selector = 'deps'
            if self.selector == 'deps':
                selector = 'required_by'

            for svc in self.services[service][selector]:
                if svc not in self.completed:
                    resolved = self.resolved(svc)
                    if resolved:
                        job = self.loop.create_task(self.act(svc))
                        services_to_act.append(job)

            await asyncio.gather(*services_to_act, return_exceptions=True)

    async def do_async(self):
        jobs = []
        if self.action == 'stop':
            self.selector = 'required_by'

        for s in self.services:
            if len(self.services[s][self.selector]) == 0:
                job = self.loop.create_task(self.act(s))
                jobs.append(job)

        await asyncio.gather(*jobs, return_exceptions=True)

    def process_output(self):
        procesed = []
        result = []
        for output_item in self.output:
            if output_item['service'] not in procesed:
                same_time_services = [output_item['service']]
                procesed.append(output_item['service'])
                for same_time_item in self.output:
                    if same_time_item['service'] != output_item['service'] and same_time_item['time'] == output_item['time']:
                        procesed.append(same_time_item['service'])
                        same_time_services.append(same_time_item['service'])

                result.append(same_time_services)

        return result

    def do(self):
        if self.validate() is not None:
            self.logger.fatal("Validation failed")

        self.loop.run_until_complete(self.do_async())
        self.loop.close()
        return self.process_output()
