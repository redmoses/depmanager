FROM python:3.9
WORKDIR /usr/src/app
COPY requirements.pip requirements.pip
RUN pip3 install -r requirements.pip
COPY app.py app.py
COPY service_manager.py service_manager.py
ENTRYPOINT [ "python", "app.py" ]
