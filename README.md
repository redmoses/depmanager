# depmanager

A service manager for starting/stopping dependent services concurrently by reading service definition from a `yaml` file. For testing purpose I have added a `1s` delay when services are started/stopped. When starting/stopping services I also print the time when the message is logged to verify if services have started concurrently in the right order.

A sample service definition file

```yaml
---
mysql:
  hosts:
    - "hostname-rm"
hadoop-namenode:
  deps:
    - "zookeeper"
  hosts:
    - "hostname-rm"
hbase-master:
  deps:
    - "hadoop-namenode"
  hosts:
    - "hostname-rm"
fullhouse:
  deps:
    - "mysql"
    - "elasticsearch"
    - "hbase-master"
  hosts:
    - "hostname-01"
    - "hostname-02"
    - "hostname-03"
kibana:
  deps:
    - "mysql"
  hosts:
    - "hostname-rm"
dashboard:
  deps:
    - "fullhouse"
  hosts:
    - "hostname-rm"
elasticsearch:
  hosts:
    - "hostname-01"
    - "hostname-02"
    - "hostname-03"
zookeeper:
  hosts:
    - "hostname-01"
    - "hostname-02"
    - "hostname-03"

```

Starting services concurrently with the above file 

```bash
➜ docker-compose run --rm depmanager start
Creating depmanager_depmanager_run ... done
2021-04-04 21:02:04,378 [INFO ] Starting service mysql ...
2021-04-04 21:02:04,378 [INFO ] Starting service elasticsearch ...
2021-04-04 21:02:04,378 [INFO ] Starting service zookeeper ...
2021-04-04 21:02:05,380 [INFO ] Starting service kibana ...
2021-04-04 21:02:05,381 [INFO ] Starting service hadoop-namenode ...
2021-04-04 21:02:06,383 [INFO ] Starting service hbase-master ...
2021-04-04 21:02:07,385 [INFO ] Starting service fullhouse ...
2021-04-04 21:02:08,387 [INFO ] Starting service dashboard ...
```

From the above output you can see `mysql`, `elasticsearch` and `zookeeper` started concrrently in the same second `21:02:04` as they don't have any dependencies. The following services starts up as soon as their subsequent dependencies are resolved.

## Usage

```bash
usage: app.py [-h] [-f SERVICE_FILE] action

positional arguments:
  action                Start or stop services

optional arguments:
  -h, --help            show this help message and exit
  -f SERVICE_FILE, --service_file SERVICE_FILE
                        Path to service definition file
```

## Run in docker

There is a `Dockerfile` and a `docker-compose` file provided with the repository. There is a sample `services.yaml` file provided, which is also the default path for service definition file for `depmanager`.

### Start

If this is the first time you are running the below command, then it will first build the docker image for `depmanager` and then start the container with `start` parameter

```bash
docker-compose run --rm depmanager start
```

### Stop

```bash
docker-compose run --rm depmanager stop
```

### Run with a different file other than the default `service.yaml`

Create a file called `services2.yaml` in the root of the repository directory. Then execute the below to start the services defined in `services2.yaml`

```bash
docker-compose run --rm depmanager start -f services2.yaml
```

## Run locally

To run locally you need to install a virtual environment with `python 3.9` and then run `pip install -r requirements.pip` after activating the environment. After this you should be able to execute the `depmanager` as below - 

```bash
python app.py start
```

# Development

## Setup virtual environment

> To create the required virtual environment you need to have `virtualenv` installed in your system

Create the virtual environment and install the required packages

```bash
# run the below commands from the root of the project directory
virtualenv -p `which python3` .virtualenv
source .virtualenv/bin/activate
pip install -r requirements.pip
```

## VS Code Setup

Install the below extensions

- [`Python`](https://github.com/Microsoft/vscode-python)
- [`Pylance`](https://github.com/microsoft/pylance-release)

For workspace settings put the below -

```json
{
    "python.pythonPath": ".virtualenv/bin/python",
    "python.autoComplete.extraPaths": [
        "depmanager/"
    ],
    "python.analysis.extraPaths": [
        "depmanager/"
    ]
}
```
